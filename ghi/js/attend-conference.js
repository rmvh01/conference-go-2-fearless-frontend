window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference')
    const url = 'http://localhost:8000/api/conferences/'
    const response = await fetch(url)
    if (!response.ok) {
        console.log("url not ok")
    } else {
        const data = await response.json()
        for (let conference of data.conferences) {
            const option = document.createElement('option')
            option.value = conference.href
            option.innerHTML = conference.name
            selectTag.appendChild(option)
        }
        const spinnerTag = document.getElementById("loading-conference-spinner")
        spinnerTag.classList.add('d-none')
        selectTag.classList.remove('d-none')
    }
    const attendeeFormTag = document.getElementById('create-attendee-form')
    attendeeFormTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(attendeeFormTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const attendee_url = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const attendee_url_response = await fetch(attendee_url, fetchConfig)
        if (!attendee_url_response.ok) {
            console.log("attendee url not ok")
        } else {
            const newAttendee = await attendee_url_response.json()
            const successTag = document.getElementById("success-message")
            attendeeFormTag.classList.add('d-none')
            successTag.classList.remove('d-none')
        }
    })
})
