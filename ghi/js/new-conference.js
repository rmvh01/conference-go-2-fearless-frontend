
window.addEventListener('DOMContentLoaded', async() => {
    const location_url = 'http://localhost:8000/api/locations/'
    const url_response = await fetch(location_url)
    if (!url_response.ok) {
        console.log("locations api url not ok")
    } else {
        const locations = await url_response.json()
        const selectTag = document.getElementById("location")
        for (const location of locations.locations) {
            let optionCard = (name, id) => `<option selected value="${id}">${name}</option>`
            let html = optionCard(location.name, location.id)
            selectTag.innerHTML += html
        }

        const formTag = document.getElementById('create-conference-form')
        formTag.addEventListener('submit', async event => {
            event.preventDefault()
            const formData = new FormData(formTag)
            const json = JSON.stringify(Object.fromEntries(formData))
            const conference_url = 'http://localhost:8000/api/conferences/'
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            const conferences_url_response = await fetch(conference_url, fetchConfig)
            if (!conferences_url_response.ok) {
                console.log("conferences api url response not ok")
            } else {
                formTag.reset()
                const newConference = await conferences_url_response.json()
            }
        })
    }
})


// <option selected value="">Choose a location</option>
