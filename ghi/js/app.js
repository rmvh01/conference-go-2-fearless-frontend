// contains code for the main page

function createCard(title, image, description, location, starts, ends) {
    return `
    <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${image}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">${title}</h5>
            <h6 class="card-subtitle mb-2 text-body-secondary">${location}</h6>
            <p class="card-text">${description}</p>
            <div class="card-footer">${new Date(starts).toDateString()} ⇒ ${new Date(ends).toDateString()}</div>
        </div>
    </div><br>
    `}



window.addEventListener('DOMContentLoaded', async () => {
    const conferences_url = 'http://localhost:8000/api/conferences/'
    try {
        const conf_url_response = await fetch(conferences_url)
        if (!conf_url_response.ok) {
            console.log("Bad URL for conference list")
        } else {
            const conferences = await conf_url_response.json()
            let col_id = 1
            for (let conference of conferences.conferences) {
                const conf_detail_url = `http://localhost:8000${conference.href}`
                const conf_detail_response = await fetch(conf_detail_url)
                if (!conf_detail_response.ok) {
                    console.log("Bad URL for conference details")
                } else {
                    const conference_details = await conf_detail_response.json()
                    console.log(conference_details)
                    // getting conference details from api
                    const title = conference_details.conference.name
                    const image = conference_details.conference.location.picture_url
                    const description = conference_details.conference.description
                    const location = conference_details.conference.location.name
                    console.log(".")
                    const starts = conference_details.conference.starts
                    console.log("..")
                    const ends = conference_details.conference.ends
                    // calling function to create and get html
                    const html = createCard(title, image, description, location, starts, ends)

                    if (col_id === 1) {
                        const col = document.getElementById("col1")
                        col.innerHTML += html
                        col_id ++
                    } else {
                        if (col_id === 2) {
                            const col = document.getElementById("col2")
                            col.innerHTML += html
                            col_id ++
                        } else {
                            const col = document.getElementById("col3")
                            col.innerHTML += html
                            col_id = 1
                        }
                    }
                }
                }
            }
        } catch(e) {
            console.log("error caught?")
    }
})

//     try {
//         const response = await fetch(url)
//         if (!response.ok) {
//             console.log("failed")
//         } else {
//             const data = await response.json()
//             console.log("ok")
//             const conference = data.conferences[0]
//             console.log("ok")
//             const nameTag = document.querySelector('.card-title')
//             console.log("ok")
//             nameTag.innerHTML = conference.name

//         }

//     } catch (e) {
//         // something
//     }
// });
{/*  */}
