
// fills the dropdown menu for selecting a state

window.addEventListener('DOMContentLoaded', async() => {
    const states_url = 'http://localhost:8000/api/states/'
    const url_response = await fetch(states_url)
    if (!url_response.ok) {
        console.log("Bad location url")
    } else {
        const location_data = await url_response.json()
        const selectTag = document.getElementById("state")
        for (let state of location_data.states) {
            let optionCard = (name, abbreviation) => `<option value="${abbreviation}">${name}</option>`
            let html = optionCard(state.name, state.abbreviation)
            selectTag.innerHTML += html }

        const formTag = document.getElementById('create-location-form')
        formTag.addEventListener('submit', async event => {
            event.preventDefault()

            // This line was added because the tag wasn't updating properly
            const updatedValue = document.getElementById('state').value
            // We will create a FormData object from the form element (formTag).
            const formData = new FormData(formTag)
            // getting the updated value into the form data
            formData.set('state', updatedValue)
            const json = JSON.stringify(Object.fromEntries(formData))
            const locationUrl = 'http://localhost:8000/api/locations/'
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            const locations_url_response = await fetch(locationUrl, fetchConfig)
            if (!locations_url_response.ok) {
                console.log('locations api url not ok')
            } else {
                formTag.reset()
                const newLocation = await locations_url_response.json()
        }
    })
    }
})
